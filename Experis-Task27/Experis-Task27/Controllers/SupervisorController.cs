﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Experis_Task27.Models;
namespace Experis_Task27.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;
        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetSupervisor()
        {
            //return new Supervisor{ Id=1,Name="Micheal Jackson",Level="Pro"};
            return _context.supervisors;
        }
    }
}